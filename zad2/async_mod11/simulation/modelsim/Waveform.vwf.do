vlog -work work C:/Users/mumot/Documents/ptc/AsyncMod11/simulation/modelsim/Waveform.vwf.vt
vsim -novopt -c -t 1ps -L cycloneii_ver -L altera_ver -L altera_mf_ver -L 220model_ver -L sgate work.AsyncMod11_vlg_vec_tst
onerror {resume}
add wave {AsyncMod11_vlg_vec_tst/i1/clock}
add wave {AsyncMod11_vlg_vec_tst/i1/out}
add wave {AsyncMod11_vlg_vec_tst/i1/out[3]}
add wave {AsyncMod11_vlg_vec_tst/i1/out[2]}
add wave {AsyncMod11_vlg_vec_tst/i1/out[1]}
add wave {AsyncMod11_vlg_vec_tst/i1/out[0]}
add wave {AsyncMod11_vlg_vec_tst/i1/reset}
run -all
