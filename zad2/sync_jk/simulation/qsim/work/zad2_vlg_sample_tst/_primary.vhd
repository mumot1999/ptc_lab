library verilog;
use verilog.vl_types.all;
entity zad2_vlg_sample_tst is
    port(
        clock           : in     vl_logic;
        sampler_tx      : out    vl_logic
    );
end zad2_vlg_sample_tst;
