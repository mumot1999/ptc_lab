library verilog;
use verilog.vl_types.all;
entity zad2 is
    port(
        \out\           : out    vl_logic_vector(2 downto 0);
        clock           : in     vl_logic
    );
end zad2;
