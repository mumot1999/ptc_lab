library ieee;
use ieee.std_logic_1164.all;

entity rejestr1bit is 
port(
	clk: in std_logic;
	rst: in std_logic;
	ain: in std_logic;
	aout: out std_logic
);
end rejestr1bit;

architecture struct of rejestr1bit is
begin

	process(clk, rst)
	begin 
		if(rst='1') then 
			aout <= '0';
		elsif rising_edge(clk) then	
			aout <= ain;
		end if;
	end process;
		
end struct;
	
