LIBRARY ieee;
USE ieee.std_logic_1164.all;

entity zad5_sumator_pelny is port (
	SW: in std_logic_vector(17 downto 16);
	LEDR: out std_logic_vector(0 to 1));
end zad5_sumator_pelny;

architecture struct of zad5_sumator_pelny is
	
	component sumator 
		port(a: in std_logic; b: in std_logic; s: out std_logic; cout: out std_logic);
	end component;
	
	begin
	sum1: sumator port map (SW(17), SW(16), LEDR(1), LEDR(0));
	
end struct;