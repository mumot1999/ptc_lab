library ieee;
use ieee.std_logic_1164.all;

entity adder_seq is 
	generic(n: natural);
	port(
	a_in, b_in: in std_logic_vector(n-1 downto 0);
	clk, rst, sel, addsub: in std_logic;
	result: out std_logic_vector(n-1 downto 0);
	overflow: out std_logic
	);
end adder_seq;

architecture struct of adder_seq is
	component adder_nbit
		generic(n: natural := n);
		port(
			a: in std_logic_vector(n-1 downto 0);
			b: in std_logic_vector(n-1 downto 0);
			carry_in: in std_logic;
			s: out std_logic_vector(n-1 downto 0);
			carry: out std_logic
		);
	end component;
	
	component register_nbit
		generic(n: natural := n);
		port(
			clk, rst: in std_logic;
			ain: in std_logic_vector(n-1 downto 0);
			aout: out std_logic_vector(n-1 downto 0)
		);
	end component;
	
	component rejestr1bit
		port(clk, rst: in std_logic; ain: in std_logic; aout: out std_logic);
	end component;
	
	type vector_array is array (4 downto 0) of std_logic_vector(n-1 downto 0);
	signal vectors: vector_array;
	
	signal carry_out: std_logic;
	
	signal add_a_value, add_b_value: std_logic_vector(n-1 downto 0);
	signal reg_addsub: std_logic;
	signal p_overflow: std_logic;
	signal reg_sel: std_logic;
	
	begin
		rejestrA: register_nbit port map (clk, rst, a_in, vectors(0));
		rejestrB: register_nbit port map (CLK, rst, b_in, vectors(1));
		
		rejestr3: register_nbit port map (CLK, rst, vectors(2), vectors(3));
		
		reg_subadd: rejestr1bit port map (clk, rst,addsub, reg_addsub);
		
		dff: rejestr1bit port map (CLK, rst, p_overflow, overflow);
		dff1: rejestr1bit port map (CLK, rst, sel, reg_sel);

		loop_b: for i in n-1 downto 0 generate
			add_b_value(i) <= vectors(1)(i) xor reg_addsub;
		end generate;
		
		with reg_sel select
			add_a_value <= vectors(0) when '0',
								vectors(3) when '1';
		
		
			
		sum0: adder_nbit port map (
			add_a_value,
			add_b_value,
			reg_addsub,
			vectors(2),
			carry_out
		);
		
		result <= vectors(3);
		
		
		p_overflow <= add_a_value(n-1) xor carry_out xor vectors(2)(n-1) xor add_b_value(n-1);--vectors(3)(n-1) xor add_a_value(n-1) xor add_b_value(n-1); 
		
		
		
end struct;