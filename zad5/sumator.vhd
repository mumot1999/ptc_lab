library ieee;
use ieee.std_logic_1164.all;

entity sumator is 
port(
	a: in std_logic;
	b: in std_logic;
	cin: in std_logic;
	s: out std_logic;
	cout: out std_logic
);
end sumator;

architecture struct of sumator is
	signal s1: std_logic;
	
	begin
		s1 <= (a and not b) or (not a and b);
		s <= (s1 and not cin) or (not s1 and cin);
		cout <= (a and b) or (s1 and cin);
end struct;
	

