library ieee;
use ieee.std_logic_1164.all;


entity zad5_5 is 
	generic(n: natural := 4);
	port(
	key: in std_logic_vector(3 downto 0);
	SW: in std_logic_vector(7 downto 0);
	hex0, hex1, hex5, hex7: out std_logic_vector(6 downto 0);
	LEDR: out std_logic_vector(7 downto 0)
	);
end zad5_5;


architecture struct of zad5_5 is

	component adder_nbit
		generic(n: natural := n);
		port(
			a: in std_logic_vector(n-1 downto 0);
			b: in std_logic_vector(n-1 downto 0);
			s: out std_logic_vector(n-1 downto 0);
			carry: out std_logic;
			carry_in: in std_logic := '0'
		);
	end component;
	
	component register_nbit is 
		generic(n: natural := n);
		port(
			clk, rst: in std_logic;
			ain: in std_logic_vector(n-1 downto 0);
			aout: out std_logic_vector(n-1 downto 0)
		);
	end component;
	
	component display_hex is 
		port(
			a: in std_logic_vector(3 downto 0);
			display: out std_logic_vector(6 downto 0)
		);
	end component;
	
	type vector8_array is array (4 downto 0) of std_logic_vector((n*2)-1 downto 0);
	type vector4_array is array (4 downto 0) of std_logic_vector(n-1 downto 0);

	signal a: std_logic_vector(n-1 downto 0);
	signal b: std_logic_vector(n-1 downto 0);
	
	signal a_reg, b_reg: std_logic_vector(n-1 downto 0);
	signal temp_a, temp_b, temp_res: vector4_array;
	signal temp_a1, temp_a2: vector4_array;

	signal clk, rst: std_logic;
	signal result: std_logic_vector((n*2)-1 downto 0);
	signal result_reg: std_logic_vector((n*2)-1 downto 0);
	signal carry_last: std_logic_vector(n-1 downto 0);
	
	begin
		clk <= key(1);
		rst <= not key(2);
		a <= SW(n-1 downto 0);
		b <= SW((n*2)-1 downto n);
		a_reg_i: register_nbit port map (clk, rst, a, a_reg);
		b_reg_i: register_nbit port map (clk, rst, b, b_reg);
		result_reg_i: register_nbit generic map(8) port map (clk, rst, result, result_reg);
		
		b_loop: for i in 0 to n-1 generate
			
			if_label_a1: if i = 1 generate
				temp_a(i)(n-1) <= '0';
				temp_a(i)(2 downto 0) <= temp_res(i-1)(3 downto 1);
			end generate;
			
			if_label_a: if i > 1 generate
				temp_a(i)(n-1) <= carry_last(i-1);
				temp_a(i)(2 downto 0) <= temp_res(i-1)(3 downto 1);
			end generate;
			
			a_loop1: for j in 0 to n-1 generate
				temp_b(i)(j) <= a_reg(j) and b_reg(i); --adder B in 
			end generate;
			
			if_label0: if i = 0 generate
				result(i) <= temp_b(i)(0);
				temp_res(i) <= temp_b(i); --last result
			end generate;
				
			if_label1: if i > 0 and i < n-1 generate
				add0: adder_nbit port map (temp_a(i), temp_b(i), temp_res(i), carry_last(i));
				result(i) <= temp_res(i)(0);
			end generate;
			
			if_label2: if i = n-1 generate
				add0: adder_nbit port map (temp_a(i), temp_b(i), temp_res(i), carry_last(i));
				result( (n*2)-2 downto (n*2)-2-i) <= temp_res(i);
				result( (n*2)-1 ) <= carry_last(i);
			end generate;

		end generate;
		LEDR <= result_reg;
		display0: display_hex port map (result_reg(3 downto 0), hex0);
		display1: display_hex port map (result_reg(7 downto 4), hex1);
		display2: display_hex port map (a, hex5);
		display3: display_hex port map (b, hex7);

		

end struct;