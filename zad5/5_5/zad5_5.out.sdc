## Generated SDC file "zad5_5.out.sdc"

## Copyright (C) 1991-2013 Altera Corporation
## Your use of Altera Corporation's design tools, logic functions 
## and other software and tools, and its AMPP partner logic 
## functions, and any output files from any of the foregoing 
## (including device programming or simulation files), and any 
## associated documentation or information are expressly subject 
## to the terms and conditions of the Altera Program License 
## Subscription Agreement, Altera MegaCore Function License 
## Agreement, or other applicable license agreement, including, 
## without limitation, that your use is for the sole purpose of 
## programming logic devices manufactured by Altera and sold by 
## Altera or its authorized distributors.  Please refer to the 
## applicable agreement for further details.


## VENDOR  "Altera"
## PROGRAM "Quartus II"
## VERSION "Version 13.0.1 Build 232 06/12/2013 Service Pack 1 SJ Web Edition"

## DATE    "Mon Jan 27 20:02:53 2020"

##
## DEVICE  "EP2C35F672C6"
##


#**************************************************************
# Time Information
#**************************************************************

set_time_format -unit ns -decimal_places 3



#**************************************************************
# Create Clock
#**************************************************************

create_clock -name {zegar} -period 20.000 -waveform { 0.000 10.000 } [get_ports {key[1]}]


#**************************************************************
# Create Generated Clock
#**************************************************************



#**************************************************************
# Set Clock Latency
#**************************************************************



#**************************************************************
# Set Clock Uncertainty
#**************************************************************



#**************************************************************
# Set Input Delay
#**************************************************************

set_input_delay -add_delay  -clock [get_clocks {zegar}]  2.000 [get_ports {SW[0]}]
set_input_delay -add_delay  -clock [get_clocks {zegar}]  2.000 [get_ports {SW[1]}]
set_input_delay -add_delay  -clock [get_clocks {zegar}]  2.000 [get_ports {SW[2]}]
set_input_delay -add_delay  -clock [get_clocks {zegar}]  2.000 [get_ports {SW[3]}]
set_input_delay -add_delay  -clock [get_clocks {zegar}]  2.000 [get_ports {SW[4]}]
set_input_delay -add_delay  -clock [get_clocks {zegar}]  2.000 [get_ports {SW[5]}]
set_input_delay -add_delay  -clock [get_clocks {zegar}]  2.000 [get_ports {SW[6]}]
set_input_delay -add_delay  -clock [get_clocks {zegar}]  2.000 [get_ports {SW[7]}]
set_input_delay -add_delay  -clock [get_clocks {zegar}]  2.000 [get_ports {key[1]}]
set_input_delay -add_delay  -clock [get_clocks {zegar}]  2.000 [get_ports {key[2]}]


#**************************************************************
# Set Output Delay
#**************************************************************

set_output_delay -add_delay  -clock [get_clocks {zegar}]  1.000 [get_ports {hex0[0]}]
set_output_delay -add_delay  -clock [get_clocks {zegar}]  1.000 [get_ports {hex0[1]}]
set_output_delay -add_delay  -clock [get_clocks {zegar}]  1.000 [get_ports {hex0[2]}]
set_output_delay -add_delay  -clock [get_clocks {zegar}]  1.000 [get_ports {hex0[3]}]
set_output_delay -add_delay  -clock [get_clocks {zegar}]  1.000 [get_ports {hex0[4]}]
set_output_delay -add_delay  -clock [get_clocks {zegar}]  1.000 [get_ports {hex0[5]}]
set_output_delay -add_delay  -clock [get_clocks {zegar}]  1.000 [get_ports {hex0[6]}]
set_output_delay -add_delay  -clock [get_clocks {zegar}]  1.000 [get_ports {hex1[0]}]
set_output_delay -add_delay  -clock [get_clocks {zegar}]  1.000 [get_ports {hex1[1]}]
set_output_delay -add_delay  -clock [get_clocks {zegar}]  1.000 [get_ports {hex1[2]}]
set_output_delay -add_delay  -clock [get_clocks {zegar}]  1.000 [get_ports {hex1[3]}]
set_output_delay -add_delay  -clock [get_clocks {zegar}]  1.000 [get_ports {hex1[4]}]
set_output_delay -add_delay  -clock [get_clocks {zegar}]  1.000 [get_ports {hex1[5]}]
set_output_delay -add_delay  -clock [get_clocks {zegar}]  1.000 [get_ports {hex1[6]}]
set_output_delay -add_delay  -clock [get_clocks {zegar}]  1.000 [get_ports {hex5[0]}]
set_output_delay -add_delay  -clock [get_clocks {zegar}]  1.000 [get_ports {hex5[1]}]
set_output_delay -add_delay  -clock [get_clocks {zegar}]  1.000 [get_ports {hex5[2]}]
set_output_delay -add_delay  -clock [get_clocks {zegar}]  1.000 [get_ports {hex5[3]}]
set_output_delay -add_delay  -clock [get_clocks {zegar}]  1.000 [get_ports {hex5[4]}]
set_output_delay -add_delay  -clock [get_clocks {zegar}]  1.000 [get_ports {hex5[5]}]
set_output_delay -add_delay  -clock [get_clocks {zegar}]  1.000 [get_ports {hex5[6]}]
set_output_delay -add_delay  -clock [get_clocks {zegar}]  1.000 [get_ports {hex7[0]}]
set_output_delay -add_delay  -clock [get_clocks {zegar}]  1.000 [get_ports {hex7[1]}]
set_output_delay -add_delay  -clock [get_clocks {zegar}]  1.000 [get_ports {hex7[2]}]
set_output_delay -add_delay  -clock [get_clocks {zegar}]  1.000 [get_ports {hex7[3]}]
set_output_delay -add_delay  -clock [get_clocks {zegar}]  1.000 [get_ports {hex7[4]}]
set_output_delay -add_delay  -clock [get_clocks {zegar}]  1.000 [get_ports {hex7[5]}]
set_output_delay -add_delay  -clock [get_clocks {zegar}]  1.000 [get_ports {hex7[6]}]


#**************************************************************
# Set Clock Groups
#**************************************************************



#**************************************************************
# Set False Path
#**************************************************************



#**************************************************************
# Set Multicycle Path
#**************************************************************



#**************************************************************
# Set Maximum Delay
#**************************************************************



#**************************************************************
# Set Minimum Delay
#**************************************************************



#**************************************************************
# Set Input Transition
#**************************************************************

