library ieee;
use ieee.std_logic_1164.all;

entity zad5_sumator_8bit is 
	port(
	SW: in std_logic_vector(15 downto 0);
	LEDR: out std_logic_vector(7 downto 0)
	);
end zad5_sumator_8bit;

architecture struct of zad5_sumator_8bit is
	component adder_nbit
		generic(n: natural:= 8);
		port(
		a: in std_logic_vector(7 downto 0);
		b: in std_logic_vector(7 downto 0);
		carry_in: in std_logic;
		s: out std_logic_vector(7 downto 0)
	);
	end component;
	
	begin
		sum0: adder_nbit port map (
			SW(15 downto 8),
			SW(7 downto 0), 
			'0',
			LEDR(7 downto 0)
		);
end struct;