library ieee;
use ieee.std_logic_1164.all;

entity display_hex is 
port(
	a: in std_logic_vector(3 downto 0):="0000";
	display: out std_logic_vector(6 downto 0)
);
end display_hex;

ARCHITECTURE strukturalna OF display_hex IS
signal b0: std_logic := (not a(3) and not a(2) and not a(1) and not a(0));
signal b1: std_logic := (not a(3) and not a(2) and not a(1) and a(0));
signal b2: std_logic := (not a(3) and not a(2) and a(1) and not a(0));
signal b3: std_logic := (not a(3) and not a(2) and a(1) and a(0));
signal b4: std_logic := (not a(3) and a(2) and not a(1) and not a(0));
signal b5: std_logic := (not a(3) and a(2) and not a(1) and a(0));
signal b6: std_logic := (not a(3) and a(2) and a(1) and not a(0));
signal b7: std_logic := (not a(3) and a(2) and a(1) and a(0));
signal b8: std_logic := (a(3) and not a(2) and not a(1) and not a(0));
signal b9: std_logic := (a(3) and not a(2) and not a(1) and a(0));
signal ba: std_logic := (a(3) and not a(2) and a(1) and not a(0));
signal bb: std_logic := (a(3) and not a(2) and a(1) and a(0));
signal bc: std_logic := (a(3) and a(2) and not a(1) and not a(0));
signal bd: std_logic := (a(3) and a(2) and not a(1) and a(0));
signal be: std_logic := (a(3) and a(2) and a(1) and not a(0));
signal bf: std_logic := (a(3) and a(2) and a(1) and a(0));

BEGIN

	display(6) <= b0 or b1 or b7 or bc;
	display(5) <= b1 or b2 or b3 or b7 or bd;
	display(4) <= b1 or b3 or b4 or b5 or b7 or b9;
	display(3) <= b1 or b4 or b7 or ba or bf;
	display(2) <= b2 or bc or be or bf;
	display(1) <= b5 or b6 or bb or bc or be or bf;
	display(0) <= b1 or b4 or bb or bd;

END strukturalna;