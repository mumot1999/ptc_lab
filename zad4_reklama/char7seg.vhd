-- IMPLEMENTACJA TRANSKODERA
LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY char7seg IS
	PORT (
		C : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
		Display : OUT STD_LOGIC_VECTOR(0 TO 6)
	);
END char7seg;

ARCHITECTURE strukturalna OF char7seg IS
BEGIN
	with C select 
		Display <= 	"1111111" when "000", --spacja
						"0000110" when "001", --E
						"0001110" when "010", --F
						"0000010" when "011", --G
						"0001001" when "100", --H
						"1001111" when "101", --I
						"0000000" when others;
END strukturalna;